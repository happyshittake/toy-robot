const fs = require('fs')
const robot = require('./robot').Robot

function validateInput (argString) {
  return new Promise((resolve, reject) => {
    let splitArr = argString.split('.')

    if (splitArr.length === 1 && splitArr[0] === argString) {
      reject(Error('txt file is not valid'))
      return
    }

    if (splitArr[splitArr.length - 1] !== 'txt') {
      reject(Error('parser only accept .txt file'))
      return
    }

    resolve(argString)
  })
}

function getFile (filename, cb) {
  return new Promise((resolve, reject) => {
    validateInput(filename)
      .then(validatedName => {
        fs.readFile(validatedName, {encoding: 'utf-8'}, (err, data) => {
          if (err) {
            reject(err)
          }

          if (!data.length) {
            reject(Error('file cannot be empty'))
          }

          resolve(data.toString().split('\n'))
        })
      })
      .catch(err => reject(err))
  })
}

function parsePlaceInstruction (instructionArr) {
  let placeArgs = instructionArr[1].split(',')

  let x = parseInt(placeArgs[0], 10)
  let y = parseInt(placeArgs[1], 10)
  let direction = placeArgs[2]

  if (isNaN(x) || isNaN(y) || !robot.isValidDirection(direction)) {
    return null
  }

  return {cmd: 'place', args: {x, y, direction}}
}

function parseNoArgsInstruction (instructionStr) {
  if (['move', 'left', 'right', 'report'].indexOf(instructionStr) >= 0) {
    return {
      cmd: instructionStr,
      args: null
    }
  }

  return null
}

function parseArgs (args) {
  return new Promise((resolve, reject) => {
    if (!args.length) {
      reject(Error('no args found'))
      return
    }

    const parsedArgs = args
      .map(instruction => instruction.toLowerCase())
      .reduce((list, instruction) => {
        let instructionObj
        const splittedInstruction = instruction.split(' ')

        if (splittedInstruction.length > 1 && splittedInstruction[0] === 'place') {
          instructionObj = parsePlaceInstruction(splittedInstruction)
        } else {
          instructionObj = parseNoArgsInstruction(splittedInstruction[0])
        }

        if (instructionObj) {
          list.push(instructionObj)
        }

        return list
      }, [])

    if (!parsedArgs.length) {
      reject(Error('no valid instructions'))
      return
    }

    resolve(parsedArgs)
  })
}

module.exports = function (filepath) {
  return getFile(filepath).then(instructionArr => parseArgs(instructionArr))
}

const MAX_X = 4
const MAX_Y = 4

exports.isValidPosition = (x, y) => {
  return !(x > MAX_X || x < 0 || y > MAX_Y || y < 0)
}

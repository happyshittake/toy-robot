const isValidPosition = require('./table').isValidPosition

const validDirections = ['north', 'south', 'east', 'west']

exports.Robot = class Robot {
  constructor () {
    this.x = 0
    this.y = 0
    this.direction = 'north'
  }

  static isValidDirection (direction) {
    return validDirections.indexOf(direction.toLowerCase()) >= 0
  }

  place ({x, y, direction}) {
    if (!isValidPosition(x, y)) {
      console.log('invalid position')
      return
    }

    if (!Robot.isValidDirection(direction)) {
      console.log('invalid direction')
      return
    }

    this.x = x
    this.y = y
    this.direction = direction
  }

  left () {
    switch (this.direction) {
      case 'north':
        this.direction = 'west'
        break
      case 'east':
        this.direction = 'north'
        break
      case 'south':
        this.direction = 'east'
        break
      case 'west':
        this.direction = 'south'
        break
    }
  }

  right () {
    switch (this.direction) {
      case 'north':
        this.direction = 'east'
        break
      case 'east':
        this.direction = 'south'
        break
      case 'south':
        this.direction = 'west'
        break
      case 'west':
        this.direction = 'north'
        break
    }
  }

  move () {
    let newY = this.y
    let newX = this.x

    switch (this.direction) {
      case 'north':
        ++newY
        break
      case 'east':
        ++newX
        break
      case 'south':
        --newY
        break
      case 'west':
        --newX
        break
    }

    if (!isValidPosition(newX, newY)) {
      return
    }

    this.x = newX
    this.y = newY
  }

  report () {
    console.log(`${this.x}, ${this.y}, ${this.direction.toUpperCase()}`)
  }

  runInstructions (instructionList) {
    instructionList.forEach(instruction => {

      // console.log(`instruction cmd: ${instruction.cmd} args: ${instruction.args}`)

      if (instruction.args) {
        this[instruction.cmd](instruction.args)
      } else {
        this[instruction.cmd]()
      }
    })
  }
}

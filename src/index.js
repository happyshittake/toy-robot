const parser = require('./parser')
const path = require('path')
const Robot = require('./robot').Robot

function start (filename, robot) {
  parser(path.join(__dirname, filename))
    .then(instructionList => robot.runInstructions(instructionList))
    .catch(e => console.log(e))
}

start('cmd.txt', new Robot())
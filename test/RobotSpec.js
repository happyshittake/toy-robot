const expect = require('chai').expect
const Robot = require('../src/robot').Robot
const sinon = require('sinon')

describe('Robot', () => {
  let robot
  let stub

  beforeEach(() => {
    robot = new Robot()
  })

  it('should place robot correctly on the table', (done) => {
    robot.place({x: 0, y: 1, direction: 'north'})
    expect(robot.direction).to.equal('north')
    expect(robot.x).to.equal(0)
    expect(robot.y).to.equal(1)
    done()
  })

  it('should ignore place operation that not valid', (done) => {
    robot.place({x: 0, y: 1, direction: 'north'})
    robot.place({x: 5, y: 5, direction: 'south'})
    expect(robot.direction).to.equal('north')
    expect(robot.x).to.equal(0)
    expect(robot.y).to.equal(1)
    done()
  })

  it('should change direction 90 degrees left when issued left command', (done) => {
    robot.place({x: 0, y: 1, direction: 'north'})
    robot.left()
    expect(robot.direction).to.equal('west')
    done()
  })

  it('should change direction 90 degrees right when issued right command', (done) => {
    robot.place({x: 0, y: 1, direction: 'north'})
    robot.right()
    expect(robot.direction).to.equal('east')
    done()
  })

  it('should correctly move when issued move command', (done) => {
    robot.place({x: 3, y: 1, direction: 'south'})
    robot.move()
    expect(robot.y).to.equal(0)
    done()
  })

  it('should should report it\'s current position & direction when issued report command', (done) => {
    stub = sinon.stub(console, 'log')
    robot.place({x: 0, y: 1, direction: 'south'})
    robot.report()
    robot.move()
    robot.report()

    expect(stub.called).to.be.true
    expect(stub.callCount).to.equal(2)
    expect(stub.getCall(0).args[0]).to.equal('0, 1, SOUTH')
    expect(stub.getCall(1).args[0]).to.equal('0, 0, SOUTH')
    stub.restore()
    done()
  })

  it('should be able run series of instructions', (done) => {
    stub = sinon.stub(console, 'log')
    robot.runInstructions([
      {
        cmd: 'place',
        args: {x: 3, y: 3, direction: 'north'}
      },
      {
        cmd: 'report',
        args: null
      },
      {
        cmd: 'right',
        args: null
      },
      {
        cmd: 'report',
        args: null
      },
      {
        cmd: 'left',
        args: null
      },
      {
        cmd: 'report',
        args: null
      },
      {
        cmd: 'move',
        args: null
      }
    ])

    expect(stub.called).to.be.true
    expect(stub.callCount).to.equal(3)
    expect(robot.y).to.equal(4)
    expect(robot.direction).to.equal('north')
    done()
  })
})

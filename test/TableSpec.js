const expect = require('chai').expect
const isValidPosition = require('../src/table').isValidPosition

describe('table function', () => {
  it('should return true on valid position', function (done) {
    const res = isValidPosition(1, 0)
    expect(res).to.be.true
    done()
  })

  it('should return false on invalid position', (done) => {
    const res = isValidPosition(5, 2)
    expect(res).to.be.false
    done()
  })
})

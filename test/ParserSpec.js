const chai = require('chai')
const chaiPromise = require('chai-as-promised')
const parser = require('../src/parser')
const path = require('path')

chai.use(chaiPromise)

const expect = chai.expect

describe('cmd parser functionality', () => {

  it('should correctly parse txt file to cmd list', done => {
    parser(path.join(__dirname, 'data/cmd.txt'))
      .then(cmdList => {
        expect(cmdList).to.deep.equal([
          {
            cmd: 'place',
            args: {x: 3, y: 3, direction: 'north'}
          },
          {
            cmd: 'left',
            args: null
          },
          {
            cmd: 'right',
            args: null
          },
          {
            cmd: 'report',
            args: null
          }
        ])
        done()
      })
  })

  it('should throw error on parsing non txt file', done => {
    expect(parser(path.join(__dirname, 'data/cmd.xml'))).to.eventually.be.rejected.and.then(() => done())
  })
})